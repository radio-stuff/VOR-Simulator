//
//  VOR_SimulatorTests.swift
//  VOR SimulatorTests
//
//  Created by William Dillon on 1/13/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Cocoa
import XCTest


class VOR_SimulatorTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testEpsilon() {
        let epsilonF: Float  = epsilon()
        let epsilonD: Double = epsilon()
        XCTAssert(epsilonF != 0.0, "Computation of epsilon with float failed.")
        XCTAssert(epsilonD != 0.0, "Computation of epsilon with double failed.")
        
        print("Float epsilon: \(epsilonF), Double epsilon \(epsilonD)\n", terminator: "")
    }
    
    func testVectorNegation() {
        let vec1f: Vector = Vector(course: 128.0, distance: 129.0, vertChange: 130.0)
        let vec2f = -vec1f
    
        assert(vec1f.course     == -vec2f.course,     "Negating vector course failed")
        assert(vec1f.distance   ==  vec2f.distance,   "Negating vector course failed")
        assert(vec1f.vertChange == -vec2f.vertChange, "Negating vector course failed")

        let vec1d: Vector = Vector(course: 131.0, distance: 132.0, vertChange: 133.0)
        let vec2d = -vec1d
        
        assert(vec1d.course     == -vec2d.course,     "Negating vector course failed")
        assert(vec1d.distance   ==  vec2d.distance,   "Negating vector course failed")
        assert(vec1d.vertChange == -vec2d.vertChange, "Negating vector course failed")
    }
    
    func testLocationSubtraction() {
        let fLAX = Location(latitude: 33.9425003, longitude: -118.4080736, elevation: 127.7)
        let fJFK = Location(latitude: 40.6397511, longitude:  -73.7789256, elevation:  12.6)
        var course = fJFK - fLAX
        
        assert(abs(course.course     -   65.9) < 0.1, "Failed to correctly compute course from two locations")
        assert(abs(course.distance   - 2144.5) < 0.1, "Failed to correctly compute distance from two locations")
        assert(abs(course.vertChange - -115.1) < 0.1, "Failed to correctly compute vertical change from two locations")

        let dCVO = Location(latitude: 44.4971111, longitude: -123.2895278, elevation: 250)
        let dBFI = Location(latitude: 47.5300000, longitude: -122.3019722, elevation:  21)
        course = dCVO - dBFI

        assert(abs(course.course   - 193.0) < 0.1, "Failed to correctly compute course from two locations")
        assert(abs(course.distance - 186.6) < 0.1, "Failed to correctly compute distance from two locations")
        assert(abs(course.vertChange - 229) < 0.1, "Failed to correctly compute vertical change from two locations")
    }
    
    func testLocPlusVec() {
        let fLAX = Location(latitude: 33.95, longitude: -118.4, elevation: 127.7)
        let vec = Vector(course: 066, distance: 100, vertChange: 2.3)
        let newPoint = fLAX + vec
        assert(abs(newPoint.latitude  -   34.62) < 0.1 , "Failed to correctly compute Location Vector addition")
        assert(abs(newPoint.longitude - -116.55) < 0.1 , "Failed to correctly compute Location Vector addition")
        assert(abs(newPoint.elevation -  130.00) < 0.1 , "Failed to correctly compute Location Vector addition")
    }
    
    func testVORdatabase() {
        let path = "/Users/wdillon/Documents/Source Code/SDR/VOR Simulator/VOR Simulator/US_NAVAIDS_20120501_20120726.kml"
        var kml: KMLDocument?
        measureBlock({kml = KMLDocument(path: path)})
        assert(kml != nil, "Unable to open KML File")
        
    }
    
    func testFlightTrackKML() {
        let path = "/Users/wdillon/Documents/Source Code/SDR/VOR Simulator/VOR Simulator/FlightAware_N8PQ_KCVO_KBFI_20150110.kml"
        var kml: KMLDocument?
        measureBlock({kml = KMLDocument(path: path)})
        assert(kml != nil, "Unable to open KML File")
        assert(kml?.placemarks[2].track.count == 74, "Track KML didn't produce the expected number of points")
    }

    func testFlightTrackCSV() {
        let path = "/Users/wdillon/Documents/Source Code/SDR/VOR Simulator/log_150216_135941_KCVO.csv"
        var csv: CSVDocument?
        measureBlock({csv = CSVDocument(path: path)})
        assert(csv != nil, "Unable to open CSV File")
        assert(csv?.track.count == 4981, "Track CSV didn't produce the expected number of points")
    }

    func testVorDatabase() {
        let firstVOR = vors[0]
        assert(firstVOR.name == "BET ", "Failure to populate VOR database")
    }
    
    func testNearbyVors() {
        let dCVO = Location(latitude: 44.4971111, longitude: -123.2895278, elevation: 250)
        var list = [VOR]()
        
        // Prime the caches
        list = allVorsWithin(100, point: dCVO)
        // Now actually measure
        measureBlock({list = allVorsWithin(100, point: dCVO)})
        for vor in list {
            print("\(vor.description()) \n", terminator: "")
        }
        assert(list.count == 9, "Selection of nearby VORs failed.")
        assert(list[0].name == "CVO ", "Sorting of nearby VORs failed")
    }
    
    func testRadialsForTrack() {
        let trackKML = KMLDocument(path: "/Users/wdillon/Documents/Source Code/SDR/VOR Simulator/VOR Simulator/FlightAware_N8PQ_KCVO_KBFI_20150110.kml")
        let trackpoints: [(NSDate, Location)] = trackKML!.placemarks[2].track
        // Get the VORs within 300nm of the start point (to speed stuff up)
        let nearbyVors = allVorsWithin(300, point: trackpoints[0].1)
        for (time, place) in trackpoints {

            // Select the three closest VORs
            let pointVors = vorsWithin(nearbyVors, nm: 100, of: place)[0 ..< 3]

            // Get the bearings (true) from each of the VORs to the current point
            let courses = Array(pointVors.map({ (vor: VOR) -> (Vector,VOR) in return (vor.location - place,vor) }))
            
            for course in courses {
                print("\(time.description),\(place.latitude),\(place.longitude),\(course.1.name),\(course.0.course)\n", terminator: "")
            }
        }
    }
    
    func testAccumulatingErrorBasic() {
        let dCVO = Location(latitude: 44.4971111, longitude: -123.2895278, elevation: 250)
        let dBFI = Location(latitude: 47.5300000, longitude: -122.3019722, elevation:  21)
        let course = dCVO - dBFI
        
        // Get 2000 path segments
        let courseSegment = course / 2000
        var intermediateLocation = dBFI
        for _ in 0 ..< 2000 {
            intermediateLocation += courseSegment
        }
        
        // Normally, three hundredths of a degree wouldn't be a great error term
        // in this case, however, we're ignoring that the difference calculation
        // is great-circle calculation.  The course is the _initial_ course.
        // For better results, we need to divide the course into segments.
        assert(abs(intermediateLocation.latitude  - dCVO.latitude ) < 0.03, "Accumulated too much error in short path")
        assert(abs(intermediateLocation.longitude - dCVO.longitude) < 0.03, "Accumulated too much error in short path")
        assert(abs(intermediateLocation.elevation - dCVO.elevation) < 0.01, "Accumulated too much error in short path")
    }
    
    func testAccumulatingErrorGC() {
        // This time, we'll re-calculate intermediate segments.
        let fLAX = Location(latitude: 33.9425003, longitude: -118.4080736, elevation: 127.7)
        let fJFK = Location(latitude: 40.6397511, longitude:  -73.7789256, elevation:  12.6)
        var course = fJFK - fLAX
        
        // Get 2000 path segments
        var courseSegment = course / 2000
        var intermediateLocation = fLAX
        for i in 0 ..< 2000 {
            // Un-comment to see what the great-circle path looks like.
            // The intermediate distance should remain constant while
            // the course changes.
//            print("Course \(courseSegment.course), distance \(courseSegment.distance)\n")
            intermediateLocation += courseSegment
            course = fJFK - intermediateLocation
            courseSegment = course / Double(1999 - i)
        }
        
        // However, if we re-calculate the course as we go, we can get really close.
        assert(abs(intermediateLocation.latitude  - fJFK.latitude ) < 0.0000001, "Accumulated too much error in long path")
        assert(abs(intermediateLocation.longitude - fJFK.longitude) < 0.0000001, "Accumulated too much error in long path")
        assert(abs(intermediateLocation.elevation - fJFK.elevation) < 0.0000001, "Accumulated too much error in long path")

    }
}
