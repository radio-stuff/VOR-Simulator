//
//  KMLFile.swift
//  VOR Simulator
//
//  Created by William Dillon on 1/13/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Foundation


func noop() {
    
}

func processTrack(xml: NSXMLElement) -> [(NSDate, Location)]{
    let formatter = NSDateFormatter()
    formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
    formatter.dateFormat = "yyyy'-'MM'-'dd'T'HH':'mm':'ss'Z'"
    formatter.timeZone = NSTimeZone(forSecondsFromGMT: 0)
    
    // The times are sent seperately, so we'll need a place to put them
    var times = [NSDate]()
    var tempTrack = [(NSDate, Location)]()
    
    if let children: [NSXMLElement] = xml.children as? [NSXMLElement] {
        for child in children {
            let strVal: NSString = child.stringValue!
            
            switch child.name! {
            case "when":
                if let date = formatter.dateFromString(strVal as String) {
                    times.append(date)
                } else {
                    // This is a major hack, but we need something to put there
                    times.append(NSDate())
                }
                
            case "gx:coord":
                var loc = Location.nullIsland()
                var components: [String] = String(strVal).componentsSeparatedByString(" ")
                
                // Longitude and latitude are reversed relative to convention
                // this is due to a quirk of kml files, it's the order they're given.
                let longitude = (components[0] as NSString).doubleValue
                let latitude  = (components[1] as NSString).doubleValue
                let elevation = (components[2] as NSString).doubleValue
                loc = Location(latitude: latitude, longitude: longitude, elevation: elevation)
                
                // Now that we have a location, pair it with the first date
                // and add it to the track.  If there are no dates, fill them with now
                var trackPoint: (NSDate, Location)
                if times.count > 0 {
                    trackPoint = (times[0], loc)
                    times.removeAtIndex(0)
                } else {
                    trackPoint = (NSDate(), loc)
                }
                
                //tempTrack.append(trackPoint)
                chronologicalAppend(&tempTrack, new: trackPoint)
            default:
                noop()
                //                    print("Ignoring child in placemark: \(child.name)\n")
            }
        }
    }
    
    return tempTrack
}

struct Placemark {
/*
    <Placemark>
        <name>BET </name>
        <styleUrl>#radio</styleUrl>
        <description>
        BET  VORTAC BETHEL
        ...
        </description>
        <Point> <coordinates>-161.8243425,60.7848344444444,0</coordinates> </Point>
    </Placemark>
*/
    
/*
    <Placemark>
        <name>N8PQ</name>
        <description>KCVO - KBFI</description>
        <gx:Track>
            <extrude>1</extrude>
            <tessellate>1</tessellate>
            <altitudeMode>absolute</altitudeMode>
            <when>2015-01-10T13:41:29</when>
            <when>2015-01-10T13:42:30</when>
            <when>2015-01-10T13:43:31</when>
            ...
            <gx:coord>-122.32667 47.55750 152</gx:coord>
            <gx:coord>-122.32445 47.55222 122</gx:coord>
            <gx:coord>-122.31223 47.54111 30</gx:coord>
        </gx:Track>
    </Placemark>
*/
    var name: String = ""
    var description: String = ""
    var point: Location? = nil
    var track = [(NSDate, Location)]()

    init?(xml: NSXMLElement) {
//        point = Location(latitude: 0, longitude: 0, elevation: 0)
//        description = "None"
//        name = "None"
//        track = [(NSDate, Location)]()
        
        // Process the array of key, dict pairs in the master file list
        if let children: [NSXMLElement] = xml.children as? [NSXMLElement] {
            for child in children {
                let strVal: String = child.stringValue! as String

                switch child.name! {
                case "name":
                    name = strVal
                case "description":
                    description = strVal
                case "Point":
                    if let coordsElements: [NSXMLElement] = child.children as? [NSXMLElement] {
                        if let coords = coordsElements[0].stringValue {
                            var components: [String] = coords.componentsSeparatedByString(",")
                            
                            // Longitude and latitude are reversed relative to convention
                            // this is due to a quirk of kml files, it's the order they're given.
                            let longitude = (components[0] as NSString).doubleValue
                            let latitude  = (components[1] as NSString).doubleValue
                            let elevation = (components[2] as NSString).doubleValue
                            point = Location(latitude: latitude, longitude: longitude, elevation: elevation)
                        } else {
                            return nil
                        }
                    } else {
                        return nil
                    }
                case "gx:Track":
                    track = processTrack(child)
                default:
                    noop()
//                    print("Ignoring child in placemark: \(child.name)\n")
                }
            }
        }
    }
}

struct KMLDocument {
/*
    <?xml version="1.0" encoding="UTF-8"?>
    <kml xmlns="http://www.opengis.net/kml/2.2">
    <Document>
    --- Styles ---
    <name>Aeronautical Navigation AIDS</name>
    <open>1</open>
    --- Default Style? ---
    --- Placemarks ---
*/

    var name = ""
    var open = false
    var placemarks = [Placemark]()
//    var styles = [Style]()
    
    init?(path: String) {
//        var manager = NSFileManager.defaultManager()
        
        print("Opening \(path)")
        let URL  = NSURL(fileURLWithPath: path)

        do {
            let xmlRep = try NSXMLDocument(contentsOfURL: URL, options: 0)
            guard let root = xmlRep.rootElement()!.children![0] as? NSXMLElement else {
                NSLog("Root of XMLDocument not an XMLElement!")
                return nil
            }
            
            if let children: [NSXMLElement] = root.children as? [NSXMLElement] {
                for child in children {
                    let strVal: String = child.stringValue! as String
                    
                    // If the element name is "key" then the element
                    // we want to keep is the next one.
                    switch child.name! {
                    case "name":
                        self.name = strVal
                    case "open":
                        self.open = (strVal == "1" ? true : false)
                    case "Placemark":
                        if let mark = Placemark(xml: child) {
                            placemarks.append(mark)
                        }
                    default:
                        //                    print("Ignoring \(child.name) \(strVal)\n")
                        noop()
                    }
                }
            } else {
                NSLog("Unable to parse KML file.")
                return nil
            }

        } catch let error {
            NSLog("Unable to open KML file: \(error)")
            return nil
        }
    }
}