
//
//  VOR.swift
//  VOR Simulator
//
//  Created by William Dillon on 1/13/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Foundation

let vors: [VOR] = vorsFromKML(KMLDocument(path: "/Users/wdillon/Documents/Source/SDR/VOR Simulator/VOR-Simulator/US_NAVAIDS_20120501_20120726.kml")!)

struct VOR {
    let name: String       // Navaid ID
    var frequency: Float   // In Hz
    var variation: Int     // In degrees (set at epoch date)
    var epoch: Int         // Year the variation was set
    var location: Location // Location of the station
    
    func description() -> String {
        return "VOR \(name): \(location.latitude), \(location.longitude) \(location.elevation) feet. \(frequency / 1000000.0)MHz, year \(epoch) variation: \(variation)"
    }
    
    init?(placemark: Placemark) {
        name = placemark.name
        frequency = 0
        variation = 0
        epoch = 0
        location = Location.nullIsland()
        
        if placemark.point == nil {
            NSLog("A VOR must have a location value.")
            return nil
        }
        
        location = placemark.point!
        
/*
...
Mag. Variation: 14E Magnetic Epoch Year: 2010
...
Frequency: 114.10
...
*/
        
        // Process the description to scrape out frequency, variation and epoch
        for line in placemark.description.componentsSeparatedByString("\n") {
            var words = line.componentsSeparatedByString(" ")
            if words[0] == "Mag." {
                let varString = words[2] as NSString
                let W = varString.rangeOfCharacterFromSet(NSCharacterSet(charactersInString: "Ww"))
                let num = varString.stringByTrimmingCharactersInSet(NSCharacterSet(charactersInString: "EWew"))
                if let value = Int(num) {
                    variation = (W.location == NSNotFound ? value : value * -1)
                }
                
                if let temp = Int(words[6]) {
                    epoch = temp
                }
            }
            
            if words[0] == "Frequency:" {
                frequency = (words[1] as NSString).floatValue * 1000000
            }
        }
        
        // Catch non-VOR cases.
        if epoch == 0 || frequency <= 108000000 || frequency > 118000000 {
            return nil
        }
    }
}

func vorsFromKML(kml: KMLDocument) -> [VOR] {
    var retval = [VOR]()
    
    for mark in kml.placemarks {
        if let temp = VOR(placemark: mark) {
            retval.append(temp)
        }
    }
    
    return retval
}

func vorsWithin(localVors: [VOR], nm: Double, of: Location) -> [VOR] {
    var list = [(Double, VOR)]()
    
    // Perform the zip and filtering at once
    for vor in localVors {
        let vorDist = dist(of, loc2Deg: vor.location)
        if vorDist < nm {
            list.append((vorDist, vor))
        }
    }
    
//    var list: [(Double, VOR)] = localVors.map({ (vor: VOR) -> (Double, VOR) in
//        return (dist(of, loc2Deg: vor.location), vor)
//    })
    
//    list = list.filter({ (input: (Double, VOR)) -> Bool in
//        let (dist, vor) = input
//        if dist < nm {
//            return true
//        } else {
//            return false
//        }
//    })
    
//    func ascending(lhs: (Double, VOR), rhs: (Double, VOR)) -> Bool
//    {
//        let (lhs_nm, _) = lhs
//        let (rhs_nm, _) = rhs
//        return lhs_nm < rhs_nm
//    }
    
    list = list.sort({$0.0 < $1.0})
    
    return list.map({ (dist: Double, vor: VOR) -> VOR in
        return vor
    })
}

func allVorsWithin(nm: Double, point: Location) -> [VOR] {
    return vorsWithin(vors, nm: nm, of: point)
}

class VORSamples {
//    let samples: [Double]
    let rawData: UnsafePointer<Float>
    let numSamples: Int
    let fileContents: NSData!
    var startDate: NSDate? = nil
    var offset: Double? = nil
    let sampleInterval: NSTimeInterval
    let sampleRate: Int
    
    init?(path: String, offset: Double, sampleRate: Int) {
        self.sampleRate = sampleRate
        self.offset = offset
        
        print("Opening \(path)")
        // Read the file into one big string
        do {
            fileContents = try NSData(contentsOfFile: path, options: NSDataReadingOptions.DataReadingMappedIfSafe)
        } catch _ {
            fileContents = nil
        }
        
        if fileContents == nil {
            NSLog("Unable to open file: \(path)")
            rawData = nil
            sampleInterval = 0
            numSamples = 0
            return nil
        }

        rawData = UnsafePointer<Float>(fileContents.bytes)
        sampleInterval = Double(fileContents.length) / Double(sampleRate) / 2.0 / Double(sizeof(Float))
        numSamples = fileContents.length / sizeof(Float)
    }
    
    func setReference(date: NSDate) {
        if offset == nil {
            NSLog("Trying to set a reference date with no offset.")
        } else {
            startDate = NSDate(timeInterval: self.offset!, sinceDate: date)
        }
    }
    
    func radial(date: NSDate) -> (Float, Float) {
        if let sDate = startDate {
            let intervalWithin = date.timeIntervalSinceDate(sDate)

            // If the interval is negative (minus average window), we are before the track
            if intervalWithin - 0.5 < 0 {
                return (0, 0)
            }
            
            // If the interval is beyond (plus the average window), we are after the track
            if intervalWithin + 0.5 > sampleInterval {
                return (0, 0)
            }

            // Now we know that we're within the range, and we can accumulate
            // samples into an average.
            var radial: Float = 0
            var power: Float  = 0
            
            var index = Int(intervalWithin * Double(sampleRate) * 2.0)
            // Depending on how the floating point math works out, we could
            // end up bouncing back and forth between even and odd indecies
            // make sure that it's always even!
            index = index / 2
            index = index * 2
            if index % 2 != 0 {
                print("Non-even index encountered!", terminator: "")
            }
            
            for i in 0 ..< sampleRate {
                // Make absolutely sure that we don't make any invalid indecies
                if index + i * 2 < 0 { continue }
                if index + i * 2 + 1 > numSamples { continue }
            
                // Sum
                power       += rawData[index + i * 2]
                let rawPhase = rawData[index + i * 2 + 1]
                if rawPhase > 0 {
                    radial += rawPhase * 180
                } else {
                    radial += 360.0 + (rawPhase * 180)
                }
            }
            
            // Divide for average
            radial = radial / Float(sampleRate)
            power  = power  / Float(sampleRate)
            // Attempt to correct for offset (calibration)
            radial = radial - 8
            radial = (radial < 0 ? 360 - radial : radial)
            // Finished
            return (radial, power)
        }
        
        return (0, 0)
    }
}