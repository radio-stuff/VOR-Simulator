//
//  addTrackController.swift
//  VOR Simulator
//
//  Created by William Dillon on 2/27/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Cocoa

class AddTrackWindowController: NSWindowController {
    @IBOutlet weak var vorComboBox: NSComboBox!
    @IBOutlet weak var pathField: NSTextField!
    @IBOutlet weak var offsetField: NSTextField!
    @IBOutlet weak var sampleRateField: NSTextField!
    var appDelegate: AppDelegate? = nil
    
    @IBAction func loadTrack(sender: AnyObject) {
        if pathField != "" {
            if let samples = VORSamples(
                path: pathField.stringValue,
                offset: self.offsetField.doubleValue,
                sampleRate: self.sampleRateField.integerValue) {
                self.appDelegate?.addVORSamples(samples, vor: vorComboBox.stringValue)
            }
        }
        
        self.close()
    }
    
    @IBAction func openPicker(sender: AnyObject) {
        let openPanel = NSOpenPanel()
        openPanel.canChooseDirectories = false
        openPanel.allowsMultipleSelection = false
        openPanel.allowedFileTypes = ["raw", "wav"]
        
        openPanel.beginWithCompletionHandler({ (result: Int) -> Void in
            if result == NSFileHandlingPanelOKButton {
                if let url = openPanel.URL {
                    if let urlPath = url.path {
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.pathField.stringValue = urlPath
                        })
                    } else {
                        NSLog("Unable to get path from URL: \(url)")
                    }
                } else {
                    NSLog("Unable to get URL from open dialog.")
                }
            }
        })
    }

    @IBAction func cancel(sender: AnyObject) {
    }

    override func windowDidLoad() {
        // Get the VOR names
        var vorNames = vors.map({
            return $0.name
        })
        
        // Sort the VORs
        vorNames.sortInPlace({
            return ($0 as NSString).caseInsensitiveCompare($1) == NSComparisonResult.OrderedAscending
        })
        
        // Add them to the combo box
        vorComboBox.removeAllItems()
        vorComboBox.addItemsWithObjectValues(vorNames)
    }
}

extension AppDelegate {
    @IBAction func loadVORTrack(sender: AnyObject) {
        if vorWindowController == nil {
            vorWindowController = AddTrackWindowController(windowNibName: "addTrack")
            vorWindowController!.appDelegate = self
        }
        
        vorWindowController!.window?.makeKeyAndOrderFront(nil)
    }
    
    func addVORSamples(samples: VORSamples, vor: String) {
        self.samples[vor] = samples
        // Set the start date of the vor samples
        if let t = track {
            samples.setReference(t[0].0)
        }
        // Update the map
        performAnimationUpdate()
    }
}

