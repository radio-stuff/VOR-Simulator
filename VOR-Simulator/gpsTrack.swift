//
//  gpsTrack.swift
//  VOR Simulator
//
//  Created by William Dillon on 3/4/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Foundation
import MapKit

extension AppDelegate {
 
    @IBAction func importFile(sender: AnyObject) {
        let openPanel = NSOpenPanel()
        openPanel.canChooseDirectories = false
        openPanel.allowsMultipleSelection = false
        openPanel.allowedFileTypes = ["kml", "csv"]
        
        // This is the actual behavior that will be executed when the user
        // clicks OK on a file.
        let action = { (result: Int) -> Void in
            dispatch_async(dispatch_get_main_queue(), {
                if result == NSFileHandlingPanelOKButton {
                    if let url = openPanel.URL, path = url.path, pathExt = url.pathExtension {
                        switch pathExt {
                            // Flight aware KML file
                        case "kml":
                            // KML google earth file
                            if let kml = KMLDocument(path: path) {
                                if kml.placemarks.count >= 3 && kml.placemarks[2].track.count > 1 {
                                    self.track = kml.placemarks[2].track
                                } else {
                                    NSLog("KML file incompatible.  Did you get it from flight aware?")
                                }
                            } else {
                                NSLog("Unable to open KML file.")
                            }
                            // CSV Garmin log file
                        case "csv":
                            if let csv = CSVDocument(path: path) {
                                self.track = csv.track
                            } else {
                                NSLog("CSV file incompatible, only G300 and G1000 files are supported.")
                            }
                        default:
                            NSLog("Unknown file type.")
                        }
                    } else {
                        NSLog("Unable to get URL from open dialog.")
                    }
                }
                // User clicked something other than "Open"
                else {
                    return
                }
                
                var northExtent =  -90.0
                var southExtent =   90.0
                var westExtent  =  180.0
                var eastExtent  = -180.0
                
                if let inTrack = self.track {
                    for point in inTrack {
                        self.points.append(CLLocationCoordinate2D(
                            latitude:  point.1.latitude,
                            longitude: point.1.longitude))
                        
                        northExtent = (point.1.latitude > northExtent ? point.1.latitude : northExtent)
                        southExtent = (point.1.latitude < southExtent ? point.1.latitude : southExtent)
                        eastExtent  = (point.1.latitude > eastExtent  ? point.1.longitude : eastExtent)
                        westExtent  = (point.1.latitude < westExtent  ? point.1.longitude : westExtent)
                    }
                    
                    self.trackPolyline = MKPolyline(coordinates: &self.points, count: self.points.count)
                    self.mapView.addOverlay(self.trackPolyline!)
                    
                    // Get a list of VORs for the trip.  For now, a hack-y solution is to
                    // get a list of those 100nm from the center of the track
                    // We've previously checked to make sure that the track is non-empty.
                    // It's safe to force-unwrap the first and last method results
                    let centerPoint = Location(
                        latitude:  (northExtent + southExtent) / 2.0,
                        longitude: (eastExtent  + westExtent ) / 2.0,
                        elevation: 0.0)
                    let northEastCorner = Location(latitude: northExtent, longitude: eastExtent, elevation: 0.0)
                    let southWestCorner = Location(latitude: southExtent, longitude: westExtent, elevation: 0.0)
                    let range = (northEastCorner - southWestCorner).distance
                    
                    // Zoom the map into the region of the track
                    let center = CLLocationCoordinate2D(
                        latitude: centerPoint.latitude,
                        longitude: centerPoint.longitude)
                    let span   = MKCoordinateSpan(
                        latitudeDelta:  abs(northExtent - southExtent),
                        longitudeDelta: abs(eastExtent  - westExtent))
                    let region = MKCoordinateRegion(center: center, span: span)
                    self.mapView.setRegion(region, animated: true)
                    
                    // Do the annotations work
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), {
                        self.annotateVorsWithin(10 + range, of: centerPoint)
                        
                        let startDate = self.track!.first!.0
                        let endDate   = self.track!.last!.0
                        let interval  = endDate.timeIntervalSinceDate(startDate)
                        self.timeline!.maxValue = interval
                        self.timeline!.doubleValue = 0.0
                        self.timeline!.enabled = true
                        self.playButton!.enabled = true
                        dispatch_async(dispatch_get_main_queue(), { () -> Void in
                            self.updateAircraftLocation(inTrack.first!.1, course: nil)
                            self.updateVORRadials(inTrack.first!.1, date: startDate)
                        })
                        return
                    })
                    
                    // Just in case the VOR samples were loaded first
                    for vorSamples in self.samples.values {
                        vorSamples.setReference(self.track!.first!.0)
                    }
                    
                }
            })
        }
        
        openPanel.beginWithCompletionHandler(action)
    }
        
}