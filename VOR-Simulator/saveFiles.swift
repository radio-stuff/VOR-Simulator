//
//  saveFiles.swift
//  VOR Simulator
//
//  Created by William Dillon on 3/11/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Cocoa

extension AppDelegate {
    func application(sender: NSApplication, openFile filename: String) -> Bool {
        
        
        return false
    }
    
    @IBAction func saveFileWithDialog(sender: AnyObject) -> Void {
        // Get a path for saving with a dialog
        let savePanel = NSSavePanel()
        let result = savePanel.runModal()
        
        if result == NSFileHandlingPanelOKButton {
            if let url = savePanel.URL {
                // Serialize the important fields in the appDelegate into
                // one NSData that we can write to the URL.
                var fileDict = [String : AnyObject]()
                fileDict["Samples"] = samples
                
                let formatter = NSDateFormatter()
                formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
                // The date and time is split between 2 fields: "2015-02-16, 15:24:14"
                formatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"

                var trackDict = Dictionary<String, Dictionary<String, Double>>()
                for trackPoint in track! {
                    let dateString = formatter.stringFromDate(trackPoint.0)
                    trackDict[dateString] = ["Latitude":  trackPoint.1.latitude,
                                             "Longitude": trackPoint.1.longitude,
                                             "Elevation": trackPoint.1.elevation ]
                }
                
                fileDict["Track"] = trackDict
                
                let fileJSON = JSON(fileDict)
                do {
                    let data = try fileJSON.rawData(options: NSJSONWritingOptions())
                    data.writeToURL(url, atomically: true)
                } catch let error as NSError {
                    print("Unable to write file: \(error.localizedDescription)")
                }
            }
        }
    }
}