
//
//  AppDelegate.swift
//  VOR Simulator
//
//  Created by William Dillon on 1/13/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Cocoa
import MapKit

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, MKMapViewDelegate {
    // UI References
    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var mapView: MKMapView!
    // Animation controls
    var playStartTime: NSDate? = nil
    @IBOutlet weak var timeline: NSSlider!
    @IBOutlet weak var timeScale: NSComboBox!
    @IBOutlet weak var fwdButton: NSButton!
    @IBOutlet weak var revButton: NSButton!
    @IBOutlet weak var playButton: NSButton!
    // Data model
    var samples = Dictionary<String, VORSamples>()
    var track: [(NSDate, Location)]? = nil
    // Ground truth VOR views and controllers
    @IBOutlet weak var vorTableView: NSTableView!
    var vorTableViewDataSource: VorTableDataSource? = nil
    var vorRadialPolylines = Dictionary<String, MKPolyline>()
    var overlaysToRemove = [MKPolyline]() // shared
    // Observed VOR views and controllers
    @IBOutlet weak var obsTableView: NSTableView!
    var obsTableViewDataSource: ObsTableDataSource? = nil
    var obsRadialPolyLines = Dictionary<String, MKPolyline>()
    var vorWindowController: AddTrackWindowController? = nil
    // Vehicle track (GPS ground truth)
    var points = [CLLocationCoordinate2D]()
    var trackPolyline: MKPolyline? = nil
    var vehicleAnnotation: MKAnnotation? = nil
    var vehicleAnnotationView: AircraftAnnotationView? = nil
//    // Adding realtime GNU radio inputs
//    var conGnuRadioController: ConnectGnuRadioController? = nil
    
    func applicationDidFinishLaunching(aNotification: NSNotification) {
        // In the background, make one access to the vors database to initialize it
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), {
            print("Loaded \(vors.count) vors.\n", terminator: "")
            
        })
//        mapView.mapType = MKMapType.Hybrid
        
        // Create a TableViewDataSource for the VOR table
        // while there is no track (and therefore no location) it is nacent
        vorTableViewDataSource = VorTableDataSource(delegate: self, view: vorTableView)
        vorTableView.setDataSource(vorTableViewDataSource)
        // Same for the observed table
        obsTableViewDataSource = ObsTableDataSource(delegate: self, view: obsTableView)
        obsTableView.setDataSource(obsTableViewDataSource)
    }

    func applicationWillTerminate(aNotification: NSNotification) {
        // Insert code here to tear down your application
    }
    
    @IBAction func testPerformance(sender: AnyObject?) {
        let dCVO = Location(latitude: 44.4971111, longitude: -123.2895278, elevation: 250)
        var list = [VOR]()
        for _ in 0 ..< 200000 {
            list = allVorsWithin(100, point: dCVO)
        }
    }
}

