//
//  mapsContols.swift
//  VOR Simulator
//
//  Created by William Dillon on 3/4/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Foundation
import MapKit

extension AppDelegate {
    // When given a location and range, find all the vors and add annotation
    func annotateVorsWithin(nm: Double, of: Location) {
        let list = vorsWithin(vors, nm: nm, of: of)
        for vor in list {
            dispatch_async(dispatch_get_main_queue(), {
                let annotation = MKPointAnnotation()
                annotation.coordinate = CLLocationCoordinate2D(
                    latitude: vor.location.latitude,
                    longitude: vor.location.longitude)
                
                self.mapView.addAnnotation(annotation)
            })
        }
        
        if let dataSource = vorTableViewDataSource {
            dataSource.vors = list
        }

        if let dataSource = obsTableViewDataSource {
            dataSource.vors = list
        }
    }
    
    // Function returns annotation views for each annotation added to the map (mostly pins)
    func mapView(mapView: MKMapView, viewForAnnotation annotation: MKAnnotation) -> MKAnnotationView? {
        if vehicleAnnotation != nil {
            if annotation === vehicleAnnotation! {
                let annotationView = AircraftAnnotationView(annotation: vehicleAnnotation, reuseIdentifier: nil)
                vehicleAnnotationView = annotationView
                return annotationView
            }
        }
        
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: nil)
        annotationView.animatesDrop = true
        return annotationView
    }
    
    // Mapkit map overlay renderer delegate method
    func mapView(view: MKMapView, rendererForOverlay: MKOverlay) -> MKOverlayRenderer {
        // Is this the GPS track polyline?
        if /*let polyline = trackPolyline where*/ rendererForOverlay === trackPolyline {
            let renderer = MKPolylineRenderer(polyline: self.trackPolyline!)
            renderer.lineWidth = 2
            renderer.strokeColor = NSColor.redColor()
            return renderer
        }

        // Otherwise, it's a VOR trackline
        if let polyline = rendererForOverlay as? MKPolyline {
            let renderer = MKPolylineRenderer(polyline: polyline)
            renderer.lineWidth = 1
            renderer.strokeColor = NSColor.blueColor()
            return renderer
        }

        return MKPolygonRenderer()
    }
}