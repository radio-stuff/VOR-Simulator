//
//  vorTableViewDataSource.swift
//  VOR Simulator
//
//  Created by William Dillon on 3/4/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Cocoa
import MapKit

class VorTableDataSource: NSObject, NSTableViewDataSource {
    let tableView: NSTableView
    let appDelegate: AppDelegate
    
    var vors = [VOR]() {
        didSet {
            // Sort them alphabetically when they're added
            vors.sortInPlace { (lhs: VOR,  rhs: VOR) -> Bool in
                return (lhs.name as NSString).caseInsensitiveCompare(rhs.name) == NSComparisonResult.OrderedAscending
            }
            tableView.reloadData()
        }
    }

    // Simply fill-in the needed other objects
    init(delegate: AppDelegate, view: NSTableView) {
        appDelegate = delegate
        tableView = view
    }

    func numberOfRowsInTableView(tableView: NSTableView) -> Int {
        let retval = appDelegate.currentLocationAndCourse()
        if let _ = retval.0 {
            return vors.count
        } else {
            return 0
        }
    }
    
    func tableView(tableView: NSTableView, objectValueForTableColumn tableColumn: NSTableColumn?, row: Int) -> AnyObject? {
        if tableColumn == nil {
            return nil
        }

        let retval = appDelegate.currentLocationAndCourse()
        if let loc = retval.0 {
            // Get the VOR in question
            let vor = vors[row]
            
            if tableColumn?.title == "Station" {
                return vor.name
            }
            
            // Get the vector from the location to the VOR.
            let vector = loc - vor.location
            
            // Depending on what column we need...
            switch tableColumn!.title {
            case "Radial":
                return vector.course
            case "Distance":
                return vector.distance
            default:
                return nil
            }
        } else {
            return nil
        }
    }
}

class ObsTableDataSource: VorTableDataSource {
    override func tableView(tableView: NSTableView, objectValueForTableColumn tableColumn: NSTableColumn?, row: Int) -> AnyObject? {
        if tableColumn == nil { return nil }

        // Get the VOR in question
        let vor = vors[row]
            
        if tableColumn?.title == "Station" {
            return vor.name
        }
        
        // Get the radial and strength from the samples
        if let (radial, power) = appDelegate.samples[vor.name]?.radial(appDelegate.currentDate()) {
            // Depending on what column we need...
            switch tableColumn!.title {
            case "Radial":
                return radial
            case "Power":
                return power
            default:
                return nil
            }
        } else {
            return nil
        }
    }
}

extension AppDelegate {
    func updateVORRadials(location: Location, date: NSDate) {
        if let dataSource = vorTableViewDataSource {
            // Delay the removal of the line until the next frame
            // maybe this will help with flickering ??
            for overlay in overlaysToRemove {
                self.mapView.removeOverlay(overlay)
            }
            
            for vor in dataSource.vors {

                // If this VOR has an old trackline, we'll need to remove it
                // when we get around that that step.
                if let old = self.vorRadialPolylines[vor.name] {
                    overlaysToRemove.append(old)
                }
                
                // Check whether we have any data for the requested VOR
                if let vorSamples = samples[vor.name] {
                    let (radial, power) = vorSamples.radial(date)
                    
                    // If ther power is less (or equal) than 0.1, don't draw a trackline
                    // because it's basically useless for any kind of navigation
                    if power > 0.1 {
                        let vector = Vector(course: Double(radial), distance: 100, vertChange: 0)
                        let endPoint = vor.location + vector
                        var endPoints = [
                            CLLocationCoordinate2D(latitude: vor.location.latitude, longitude: vor.location.longitude),
                            CLLocationCoordinate2D(latitude: endPoint.latitude, longitude: endPoint.longitude)]
                        let polyline = MKPolyline(coordinates: &endPoints, count: endPoints.count)
                        // Add this trackline as an overlay
                        self.mapView.addOverlay(polyline)
                        // Keep track of it for later removal
                        self.vorRadialPolylines[vor.name] = polyline
                    }
                }
                
                // Create a polyline from the VOR location to a point
                // along the radial (let's say the distance + 10nm)
//                var vector = location - vor.location
//                vector.distance += 10
//                let endPoint = vor.location + vector
//                var endPoints = [
//                    CLLocationCoordinate2D(latitude: vor.location.latitude, longitude: vor.location.longitude),
//                    CLLocationCoordinate2D(latitude: endPoint.latitude, longitude: endPoint.longitude)]
//
//                let polyline = MKPolyline(coordinates: &endPoints, count: endPoints.count)
//                self.mapView.addOverlay(polyline)
//
//                if let old = self.vorRadialPolylines[vor.name] {
//                    overlaysToRemove.append(old)
//                }

//                self.vorRadialPolylines[vor.name] = polyline
            }
        }
    }
}