//
//  animation.swift
//  VOR Simulator
//
//  Created by William Dillon on 3/4/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Foundation
import MapKit

extension AppDelegate {
    // This is "the one true function" to get the current
    func currentDate() -> NSDate {
        return NSDate(timeInterval: timeline.doubleValue, sinceDate: track!.first!.0)
    }
    
    func currentLocationAndCourse() -> (Location?, Double?) {
        // To have a meaningful location or course, we need to have a track loaded
        if let loadedTrack = track where loadedTrack.count > 0 {
            // Start with the current date
            let now = currentDate()
            // the 'first' is safe to force-upwrap because we know the count
            // is one or greater.  Same goes with 'last'.
            let interval = now.timeIntervalSinceDate(loadedTrack.first!.0)
            // If current time is before or equal to the first point, clamp
            if interval <= 0 {
                // Lets do our best to try to find a course by getting the
                // track between the next point, provided that there are two
                // or more trackpoints.
                var course: Double? = nil
                if loadedTrack.count >= 2 {
                    let vector = loadedTrack[1].1 - loadedTrack[0].1
                    course = vector.course
                }
                return(loadedTrack.first!.1, course)
            }
            // If somehow we've passed the end of the track (shouldn't happen)
            // then do the same thing with the last trackpoint
            let remaining = loadedTrack.last!.0.timeIntervalSinceDate(now)
            if remaining <= 0 {
                var course: Double? = nil
                if loadedTrack.count >= 2 {
                    let vector = loadedTrack[loadedTrack.count - 2].1 - loadedTrack[loadedTrack.count - 1].1
                    course = vector.course
                }
                return(loadedTrack.last!.1, course)
            }
            // Find the trackpoints that bracket the current time.  We'll use
            // these to perform an linear interpolation of the current location
            // and we'll use it to compute the course
            var before: (NSDate, Location)! = nil
            var after: (NSDate, Location)!  = nil
            for i in 0 ..< loadedTrack.count {
                // Get the difference in time between the timestamp
                // on the track and the current time of the animation
                let testInterval = now.timeIntervalSinceDate(loadedTrack[i].0)
                // Catch the case where it's an exact match (unlikely)
                if testInterval == 0 {
                    // Try to figure out whether we can get a course (like the
                    // special cases of the ends)
                    var course: Double? = nil
                    if loadedTrack.count > (i + 1) {
                        let vector = loadedTrack[i+1].1 - loadedTrack[i].1
                        course = vector.course
                    }
                    return (loadedTrack[i].1, course)
                }
                // Otherwise, if the test interval is negative, we've passed
                // the track point that brackets our range on the late side.
                if testInterval < 0 {
                    // Save our interval and move on to interpolation
                    after = loadedTrack[i]
                    // We should make sure that we really have enough points
                    if i > 0 {
                        before = loadedTrack[i-1]
                    }
                    break
                }
            }
            // If for some crazy reason we didn't find a range:
            if before == nil { return (nil, nil) }
            // Now we have the last stupid special case.  If the after point
            // is nil, we know that the 'before' point is the last trackpoint
            // therefore, we do the exact same logic as the last trackpoint case
            if after == nil {
                var course: Double? = nil
                if loadedTrack.count >= 2 {
                    let vector = loadedTrack[loadedTrack.count - 2].1 - loadedTrack[loadedTrack.count - 1].1
                    course = vector.course
                }
                return(loadedTrack.last!.1, course)
            }
            // Lets start by finding out exactly how much time is elapsed
            // between the bracketed track points
            let sampleDelta = after.0.timeIntervalSinceDate(before.0)
            // Now, lets figure out what proportion of the interval the
            // current 'playhead' time is within the interval.
            let beforePointInterval = before.0.timeIntervalSinceDate(loadedTrack.first!.0)
            let proportionWithin = (interval - beforePointInterval) / sampleDelta
            // Derive a vector between the interpolated points
            var vector = after.1 - before.1
            // If the vector has a very small distance, the course could be NaN.
            // In this case, we can't use the vector to add to a location.
            // Also in this case, we're basically interpolating between two
            // coincident points, so the whole point of interpolation is moot
            if vector.course.isNaN { return (after.1, nil) }
            // Otherwise, scale the vector and return a new point
            else {
                // Scale the distance of the vector relative to the proportion
                vector.distance = vector.distance * proportionWithin
                // Make a new location along the vector and return it
                return (before.1 + vector, vector.course)
            }
        }
        return (nil, nil)
    }
    
    func updateAircraftLocation(location: Location, course courseOptional: Double?) {
        // If the vehicle annotation doesn't exist yet, make one
        if vehicleAnnotation == nil {
            let annotation = MKPointAnnotation()
            vehicleAnnotation = annotation
            self.mapView!.addAnnotation(vehicleAnnotation!)
        }
        // Set the location of the annotation
        if let annotation = vehicleAnnotation as? MKPointAnnotation {
            annotation.coordinate = CLLocationCoordinate2D(
                latitude:  location.latitude,
                longitude: location.longitude)
        }
        // If we got course information from this update, update the icon
        if let course = courseOptional, view = vehicleAnnotationView {
            view.course = course
        }
    }

    private func animationFunction() {
        // Detect whether we're stopped (playStartTime is nil if not playing)
        if playStartTime == nil { return }
        // Get the wall-clock time from when the user clicked 'play' at the
        // start of the track (virtually if the slider was moved)
        let timeSinceStart = NSDate().timeIntervalSinceDate(playStartTime!)
        // If the wall-clock time indicated that the time of the animation
        // has elapsed, stop playback
        if  timeSinceStart >= timeline!.maxValue {
            playStartTime = nil
        }
        // Otherwise, animate
        else {
            // Set the timeline to its new location which matches the playback time
            timeline!.doubleValue = timeSinceStart
            // Do the animation for the plane and vors
            performAnimationUpdate()
            // Once more, from the top!
            animationTimerStart()
        }
        
    }
    
    private func animationTimerStart() {
        // All this function does is wrap this async time call that calls
        // the animation function.  The animation function itself calls this
        // function.  That's the loop that keeps animation going.
        dispatch_after(1, dispatch_get_main_queue()) { () -> Void in
            self.animationFunction()
        }
    }
    
    @IBAction func togglePlay(sender: AnyObject) {
        // If we're already playing, stop.
        if playStartTime != nil {
            playStartTime = nil
        }
        // Otherwise, start playing, but only if the
        // timeline isn't already at the endstop
        else if timeline.doubleValue < timeline.maxValue {
            // Save the virtual start time as if the user clicked play in the
            // future such that the timeline slider would have naturally come
            // to match the place it's at now.
            playStartTime = NSDate(timeIntervalSinceNow: timeline!.doubleValue * -1.0)
            // Begin the animation using the async timer block
            animationTimerStart()
        }
    }
    
    func performAnimationUpdate() {
        // Try to get the new location and course of the vehicle
        let retval = currentLocationAndCourse()
        // If that succeeded, we can proceed
        if let location = retval.0 {
            // Update the location of the aircraft on the track
            updateAircraftLocation(location, course: retval.1)
            // Update the VORs to show their radials to the vehicle
            updateVORRadials(location, date: currentDate())
            // Update the data tables
            obsTableView.reloadData()
            vorTableView.reloadData()
        }
    }
    
    @IBAction func timelineMoved(sender: AnyObject) {
        // If we're animating, we need to figure out a new virtual start time
        // that matches the new position of the timeline.
        if let _ = playStartTime {
            // This is exactly the same as starting the animation
            playStartTime = NSDate(timeIntervalSinceNow: timeline!.doubleValue * -1.0)
            return
        }
        // Otherwise, just move the plane.
        else {
            performAnimationUpdate()
        }
    }
}