//
//  KMLFile.swift
//  VOR Simulator
//
//  Created by William Dillon on 1/13/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Foundation


struct placemark {
    let time: NSDate
    let latitude: Double
    let longitude: Double
    let elevation: Double
}

// This is a kinda silly insertion sort algorithm, it's optimized for lists
// that are already sorted.  In that case, there's only one additional compare.
func chronologicalAppend(inout array: [(NSDate, Location)], new: (NSDate, Location)) {
    // If the array empty, just add and move on
    if array.count == 0 {
        array.append(new)
        return
    }
    
    // Otherwise, if the last point in the array is earlier than the new date, just append.
    let interval = new.0.timeIntervalSinceDate(array.last!.0)
    if  interval >= NSTimeInterval(0) {
        array.append(new)
        return
    }
    
    // Finally, if this new date has to be in the middle somewhere, find out where.
    for i in 0..<array.count {
        let interval = new.0.timeIntervalSinceDate(array.last!.0)
        // If the new date is before a given index, insert the new object at that index.
        if  interval < NSTimeInterval(0) { array.insert(new, atIndex: i) }
    }
}

struct CSVDocument {
    var name = ""
    var open = false
    var track = Array<(NSDate, Location)>()
    
    init?(path: String) {
        print("Opening \(path)")

        // Read the file into one big string
        do {
            let fileContents = try NSString(contentsOfFile: path, usedEncoding: nil)
            // Split the file into lines (rows of the CSV)
            //let rows = split(fileContents, { $0 == "\n" }, allowEmptySlices: false)
            let NSStringRows = fileContents.componentsSeparatedByCharactersInSet(NSCharacterSet.newlineCharacterSet())
            let rows = NSStringRows
            
            // Skip the 'comment' rows
            var dataRows = rows.filter({$0[$0.startIndex] != "#"})
            
            // The first row is the column headers, we can get the indexes to the
            // columns by matching these.
            //        var header = split(dataRows[0], { $0 == "," }, allowEmptySlices: false)
            let header = (dataRows[0] as NSString).componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: ","))
            
            // Create a dictionary of labels -> indecies
            var headerDict = Dictionary<String, Int>()
            for (var i = 0; i < header.count; i++) {
                headerDict[header[i] ] = i
            }
            
            let dateIndex: Int! = headerDict["  Lcl Date"]
            let timeIndex: Int! = headerDict[" Lcl Time"]
            let latIndex: Int!  = headerDict["     Latitude"]
            let lonIndex: Int!  = headerDict["    Longitude"]
            let elevIndex: Int! = headerDict["  AltGPS"]
//            let utcIndex: Int!  = headerDict[" UTCOfst"]
            let fixIndex: Int!  = headerDict[" GPSfix"]
            
            let formatter = NSDateFormatter()
            formatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            // The date and time is split between 2 fields: "2015-02-16, 15:24:14"
            formatter.dateFormat = "yyyy'-'MM'-'dd' 'HH':'mm':'ss"
            
            // Rows 1..all are data points
            dataRows.removeAtIndex(0)
            for row in dataRows {
                //let columns = split(row, { $0 == ","}, maxSplit: header.count, allowEmptySlices: true)
                let columns: [NSString] = (row as NSString).componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: ","))
                
                // Skip rows that are short columns (happens at the end of a file, for example)
                if columns.count < header.count {
                    continue
                }
                
                // Only accept points that have a 3DDiff GPS lock
                if columns[fixIndex] != " 3DDiff" {
                    continue
                }
                
                // First, get the UTC offset for this row
// TODO: Actually compute UTC offset
//                let offsetFields: [NSString] = columns[utcIndex].componentsSeparatedByCharactersInSet(NSCharacterSet(charactersInString: ":"))
//                let utcOffset = offsetFields[0].intValue
                formatter.timeZone = NSTimeZone(forSecondsFromGMT: -7 * 60)
                
                // Create a location from the latitude and longitude
                let latString = columns[latIndex]
                let lonString = columns[lonIndex]
                let eleString = columns[elevIndex]
                let lat = latString.doubleValue
                let lon = lonString.doubleValue
                let elev = eleString.doubleValue
                let loc = Location(latitude: lat, longitude: lon, elevation: elev)
                
                // Now, concatinate the date and time, and create an NSDate
                // Only create a new track point if this succeededs
                if let date = formatter.dateFromString("\(columns[dateIndex]) \(columns[timeIndex])") {
                    // Make absolutely certain that the trackpoints are sorted in chronological order
                    chronologicalAppend(&track, new: (date, loc))
                }
            }
        } catch let error {
            NSLog("Unable to open file into a string: \(error)")
            return nil
        }
    }
}
