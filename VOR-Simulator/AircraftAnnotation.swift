//
//  AircraftAnnotation.swift
//  VOR Simulator
//
//  Created by William Dillon on 2/24/15.
//  Copyright (c) 2015 HouseDillon. All rights reserved.
//

import Cocoa
import MapKit

class AircraftAnnotationView: MKAnnotationView {
    
    var jetIcon = NSImage()
    var imageView = NSImageView()
    var course: Double? = 0.0 {
        willSet {
            if let val = newValue {
                if val.isFinite {
                    imageView.frameCenterRotation = CGFloat(val)
                }
            }
        }
    }
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
    }
    
    override init(frame frameRect: NSRect) {
        super.init(frame: CGRect(x: 0.0, y: 0.0, width: 40, height: 40))

        jetIcon  = NSImage(byReferencingFile: "/Users/wdillon/Documents/Source Code/SDR/VOR Simulator/VOR Simulator/Images/jetIcon.png")!

        let rect = CGRect(x: 0.0, y: 0.0, width: 40, height: 40)
        imageView = NSImageView(frame: rect)
        imageView.imageScaling = NSImageScaling.ScaleProportionallyUpOrDown
        self.addSubview(imageView)
        imageView.image = jetIcon
        
        course = 0.0
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
}
